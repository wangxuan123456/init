# 自定义工具类
from flask import current_app
from flask import g
from flask import session
import functools

from info.models import User


def do_index_class(index):
    # 返回索引对应的类名
    if index == 1:
        return "first"
    elif index == 2:
        return "second"
    elif index == 3:
        return "third"
    return ""




def user_login_data(f):
    @functools.wraps(f)
    def wrapper(*args,**kwargs):
        # 查看用户登录信息
        user_id = session.get("user_id", None)
        user = None
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        g.user = user
        return f(*args,**kwargs)
    return wrapper