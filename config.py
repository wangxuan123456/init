import logging
from redis import StrictRedis


class Config(object):
    """项目配置"""
    DEBUG = True
    # 为数据库加载配置
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information2702"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 在请求结束时候，如果指定此配置为 True ，那么 SQLAlchemy 会自动执行一次 db.session.commit()操作
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SECRET_KEY = "Mp6l903hdmPBHPJzkQnAWqe96riAbvpiMlQcYxyEMvrb6Nh5J7Es8bhU2XDaS7Pu"
    #redis配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = "6379"
    # Session保存配置
    SESSION_TYPE = "redis"
    # 开启session签名
    SESSION_USE_SIGNER = True
    # 指定 Session 保存的 redis
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 设置需要过期
    SESSION_PERMANENT = False
    # 设置过期时间
    PERMANENT_SESSION_LIFETIME = 86400 * 2
    #设置默认日志等级
    LOG_LEVEL = logging.DEBUG



class DevelopmentConfig(Config):
    """开发环境下项目配置"""
    DEBUG = True


class ProductionConfig(Config):
    """生产环境下的配置"""
    DEBUG = False
    #设置生产环境下日至等级
    LOG_LEVEL = logging.WARNING
    #添加链接的数据库


class TestingConfig(Config):
    """测试环境下配置"""
    DEBUG = True
    #开启testing可以找到错误代码的具体位置
    TESTING = True


config = {
    "development": DevelopmentConfig,
    "production":ProductionConfig,
    "testing":TestingConfig,
}

